"""
Start ipython with the command `ipython --matplotlib`

Then, run this script with:

```
run job_sim_predaprey.py
```
"""
import matplotlib.pyplot as plt

from pathlib import Path

from fluidsim.solvers.models0d.predaprey.solver import Simul

params = Simul.create_default_params()

params.time_stepping.deltat0 = 0.1
params.time_stepping.t_end = 30

params.output.periods_print.print_stdout = 0.01

sim = Simul(params)

sim.state.state_phys.set_var("X", sim.Xs -1) #4
sim.state.state_phys.set_var("Y", sim.Ys +3) #2
#/home/roblet/.local/lib/python3.10/site-packages/fluidsim/solvers/models0d/predaprey/output/__init__.py
#line 52 
        # return p.C * log(X) - p.D * X + p.A * log(Y) - p.B * Y

# sim.output.phys_fields.plot()
sim.time_stepping.start()

here = Path(__file__).absolute().parent
tmp_dir = here.parent / "../tmp"
tmp_dir.mkdir(exist_ok=True)

sim.output.print_stdout.plot_XY()
plt.title(r"Simulation of Lotka-Volterra evolution equation")
plt.xlabel("X, preys")
plt.ylabel("Y, predators")
fig = plt.gcf()
fig.savefig(tmp_dir / "fig_evolution_round.png")

# Note: if you want to modify the figure and/or save it, you can use
# ax = plt.gca()
# fig = ax.figure

sim.output.print_stdout.plot_XY_vs_time()
plt.title(r"Time evolution of Lotka-Volterra equation")
fig = plt.gcf()
fig.savefig(tmp_dir / "fig_evolution_round_time.png")

plt.show()
